WORDPRESS_DIRECTORY = src/wordpress
COMPOSER_BINARIES = vendor/bin

CONFIGURATION_FILE = etc/wordpress-settings.ini
WORDPRESS_LANGUAGE = $(shell grep -Po '(?<=language=).*' $(CONFIGURATION_FILE))

# Optional previous install location
PREVIOUS_INSTALL_LOCATION = $(shell grep -Po '(?<=previous_install_directory=).*' $(CONFIGURATION_FILE))

all: install

### User defined functions
define DOWNLOAD_CORE
	if [ '$(WORDPRESS_LANGUAGE)' != '' ]; \
   	then \
   		$(COMPOSER_BINARIES)/wp core download --path=$(WORDPRESS_DIRECTORY)/ --locale=$(WORDPRESS_LANGUAGE) ; \
   	else \
   		$(COMPOSER_BINARIES)/wp core download --path=$(WORDPRESS_DIRECTORY)/ ; \
	fi; \
	rm -rf $(WORDPRESS_DIRECTORY)/wp-content/plugins
endef

## Targets
clean:
	# Delete the WordPress folder if it exists
	if [ -d $(WORDPRESS_DIRECTORY) ] ; \
    then \
         rm -rf $(WORDPRESS_DIRECTORY)/ ; \
    fi;

	# Delete the vendor folder if it exists
	if [ -d "vendor/" ] ; \
    then \
         rm -rf "vendor/" ; \
    fi;

install:
	# Install the composer dependencies
	composer install

	# Update the composer dependencies
	composer update

	# Install the core of wordpress
	if [ ! -d '$(WORDPRESS_DIRECTORY)' ] ; \
	then \
		$(call DOWNLOAD_CORE) ; \
	fi;