<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    /**
     * The default configuration of the WordPress application
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/wordpress-skeleton
     */

    /**
     * The default configuration file has to exist
     */
    if (!file_exists(SYSTEM_CONFIGURATION_DIRECTORY . '/wordpress-settings.ini')) {
        throw new InvalidArgumentException('Configuration file should exist in /etc/ folder');
    }

    /**
     * Parse the default configuration file
     */
    $config = parse_ini_file(SYSTEM_CONFIGURATION_DIRECTORY . '/wordpress-settings.ini', true);

    /**
     * If the environment isn't set, set the testing environment as a default
     */
    if (!isset($config['environment'])) {
        $config['environment'] = 'testing';
    }

    /**
     * Set the WordPress language
     *
     * If it isn't set, set English as default
     */
    define('WPLANG', isset($config['language']) ? $config['language'] : 'en_US');

    /**
     * Define the WordPress home subdirectory
     */
    define('WP_HOME_SUBDIRECTORY', isset($config['WP_HOME_SUBDIRECTORY']) ? $config['WP_HOME_SUBDIRECTORY'] : '');

    /**
     * Set the server name if it doesn't exist
     */
    if (!isset($_SERVER['SERVER_NAME'])) {
        $_SERVER['SERVER_NAME'] = "";
    }

    /**
     * Configure the WordPress home
     */
    define(
        'WP_HOME',
        'http://' . $_SERVER['SERVER_NAME'] . '/' . WP_HOME_SUBDIRECTORY
    );

    /**
     * Configure the Wordpress directory
     */
    define('WP_SITEURL', WP_HOME . '/src/wordpress');

    /**
     * Set the webroot directory
     */
    define('WEBROOT_DIRECTORY', ROOT_DIRECTORY . '/src');

    /**
     * Set the content directory
     */
    define('WP_CONTENT_DIR', WEBROOT_DIRECTORY . '/content');
    define('WP_CONTENT_URL', WP_HOME . '/src/content');


    /**
     * Set the table prefix
     */
    $table_prefix = isset($config['table_prefix']) ? $config['table_prefix'] : "wp_";

    /**
     * Load the default database configuration
     */
    require_once 'default-database-configuration.php';