<?php
    /*
     * The MIT License (MIT)
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
     * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
     * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
     * permit persons to whom the Software is furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
     * Software.
     *
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
     * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
     * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

    /**
     * The base configuration file of the WordPress application
     *
     * @author    Bas van Driel <basvandriel94@gmail.com>
     * @copyright 2017 Bas van Driel
     *
     * @license   https://opensource.org/licenses/MIT The MIT License
     *
     * @see       https://github.com/basvandriel/wordpress-skeleton
     */

    /**
     * Set the absolute path to the WordPress directory.
     */
    if (!defined('ABSPATH')) {
        define('ABSPATH', dirname(__FILE__) . '/wordpress/');
    }

    /**
     * Set the file locations
     */
    define('ROOT_DIRECTORY', dirname(__DIR__));
    define('VARIABLE_DATA_DIRECTORY', ROOT_DIRECTORY . '/var');
    define('SYSTEM_CONFIGURATION_DIRECTORY', ROOT_DIRECTORY . '/etc');

    /**
     * Load the composer auto-loader
     */
    require_once ROOT_DIRECTORY . '/vendor/autoload.php';

    /**
     * Default configuration
     */
    require_once ROOT_DIRECTORY . '/src/default-configuration.php';

    /**
     * Specific environment configuration
     */
    require_once ROOT_DIRECTORY . '/src/environment-configuration.php';

    /**
     * Security configuration
     */
    require_once ROOT_DIRECTORY . '/src/security-configuration.php';

    /**
     * Sets up WordPress vars and included files.
     */
    require_once(ABSPATH . 'wp-settings.php');