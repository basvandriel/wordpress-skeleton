# `/src` : Source code

## Purpose

The `/src` hierarchy contains all object-oriented application source code.

## Requirements

Only object-oriented PHP source code compatible with the WordPress code standards.

## Links

* [Linux Foundation: FHS 3.0](http://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.html#USRSRCSOURCECODE)