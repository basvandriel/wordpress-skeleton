# wordpress-skeleton
This repository contains a pre-configured WordPress boilerplate with common settings
for better development and deployment.

The project was designed to work with an Apache server.

I started this project because I wanted to learn more about WordPress and shell scripting.

There are currently only new WordPress installations supported.

## Requirements
* An Apache web-server running PHP >= 5.6
* The `git` tool
* The unix `make` tool
* The `composer` dependency manager tool

## Installation
Clone the repository to a `htdocs/` directory from any web-server 
on your computer.

```shell
# Clones the repository
git clone https://github.com/basvandriel/wordpress-skeleton.git
```

Create a new file called `wordpress-settings.ini` in the `etc/` project subdirectory. In the file paste the following contents:

```ini
language=
envirnoment=

[database]
host=
name=
user=
password=
```

In this file in fill in the following :

### Language
At the `language` key, fill in the desired 
installation language with it's locale code.

The available locale codes can be [found here](https://wpcentral.io/internationalization/).

### Database connection credentials
At the `host` key, fill in the ip-address where the database is located. 
At the `name` key, fill in the database name the WordPress installation should use.

At the `user` and `password` key, fill in the database user credentials.

---

Make sure the connection is active and the database exists.

Run the `make` command in the root of the project. This will download the WordPress core with the the desired language and version to the `src/wordpress/` folder in the directory the repository was cloned to.

```
# Builds the MAKEFILE
make install
```
## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Authors
This project was initially created by [Bas van Driel](https://github.com/basvandriel "GitHub page") ([@bvandriel](https://twitter.com/bvandriel "Twitter page")), where [these people](https://github.com/basvandriel/WWW/graphs/contributors) contributed to it.

## Links
* [Source code](https://github.com/basvandriel/wordpress-skeleton)
* [Issue tracker](https://github.com/basvandriel/wordpress-skeleton/issues)