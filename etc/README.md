# `/etc` : Host-specific system configuration

## Purpose

The `/etc` hierarchy contains configuration files. A "configuration file" is a local file used to control the operation
of a program; it must be static and cannot be an executable binary.

## Requirements

No binaries may be located under `/etc`.

## Links

* [Linux Foundation: FHS 3.0](http://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.html#etcHostspecificSystemConfiguration)