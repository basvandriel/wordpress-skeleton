#!/bin/bash
# The root path of the project
ROOT_PATH=$( dirname $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) );

# Composer installed binaries
COMPOSER_BINARIES=${ROOT_PATH}/vendor/bin/

# WordPress installation directory
WORDPRESS_DIRECTORY=${ROOT_PATH}/src/wordpress

# TODO Check if the WordPress core is already installed
# TODO Install the WordPress core with arguments from the user

read -p "Press enter to continue"
